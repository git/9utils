# Collection of utilities inspired by Plan9
# SPDX-FileCopyrightText: 2023 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

HARE ?= hare
CRAM ?= cram

PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin/
MANDIR ?= $(PREFIX)/share/man/
MAN1DIR ?= $(MANDIR)/man1/

DESTDIR ?=

CMDS = cmd/unicode
MANS = cmd/unicode.1

all: $(CMDS)

.SUFFIXES: .ha

.ha:
	$(HARE) build $(HAREFLAGS) -o $@ $<

.PHONY: lint
lint:
	mandoc -T lint -W warning ${MANS}
	reuse lint

.PHONY: check
check: all
	$(CRAM) test-cmd/*.t

.PHONY: install
install: all
	mkdir -p ${DESTDIR}/${BINDIR}
	cp -p ${CMDS} ${DESTDIR}/${BINDIR}
	mkdir -p ${DESTDIR}/${MAN1DIR}
	cp ${MANS} ${DESTDIR}/${MAN1DIR}
